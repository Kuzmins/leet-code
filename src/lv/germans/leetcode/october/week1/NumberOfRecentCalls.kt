package lv.germans.leetcode.october.week1

//https://leetcode.com/explore/featured/card/october-leetcoding-challenge/559/week-1-october-1st-october-7th/3480/
fun main() {
    val recentCounter = NumberOfRecentCalls()
    println(recentCounter.ping(1)) // requests = [1], range is [-2999,1], return 1

    println(recentCounter.ping(100)) // requests = [1, 100], range is [-2900,100], return 2

    println(recentCounter.ping(3001)) // requests = [1, 100, 3001], range is [1,3001], return 3

    println(recentCounter.ping(9002)) // requests = [1, 100, 3001, 3002], range is [2,3002], return 3
}

class NumberOfRecentCalls() {
    private val innerList = mutableListOf<Int>()
    private val maxSize = 3000

    fun ping(t: Int): Int {
        val minRange = t - maxSize
        val lastElement = innerList.lastOrNull()
        if (lastElement != null && lastElement < minRange) {
            innerList.clear()
        }
        innerList.removeAll { it < minRange }
        innerList.add(t)
        return innerList.count()
    }
}

