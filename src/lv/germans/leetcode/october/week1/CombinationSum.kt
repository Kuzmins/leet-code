package lv.germans.leetcode.october.week1

import java.util.function.BiFunction


fun main() {
    println(CombinationSum().combinationSum(intArrayOf(2, 7, 6, 3, 5, 1), 9).joinToString())

    println(CombinationSum().combinationSum(intArrayOf(2, 3, 6, 7), 7).joinToString())
}

class CombinationSum  {
    fun combinationSum(candidates: IntArray, target: Int): List<List<Int>> {
        candidates.sort()
        return calculateCombinations(target, emptyList(), candidates, 0)
    }

    private fun calculateCombinations(target: Int, currentPossibleCandidate: List<Int>, candidates: IntArray, from: Int): MutableList<List<Int>> {
        val result = mutableListOf<List<Int>>()

        for (i in from until candidates.size) {
            val candidate = candidates[i]
            if (target - candidate == 0) {
                val possibleCandidates = currentPossibleCandidate.toMutableList()
                possibleCandidates.add(candidate)
                result.add(possibleCandidates)
            } else if (target - candidate < 0) {
                break
            } else {
                val newTarget = target - candidate
                val newCandidates = currentPossibleCandidate.toMutableList()
                newCandidates.add(candidate)

                val newResult = calculateCombinations(newTarget, newCandidates, candidates, candidates.indexOf(candidate))
                newResult.forEach { result.add(it) }
            }
        }

        return result
    }
}

