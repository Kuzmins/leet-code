package lv.germans.leetcode.october.week1

//https://leetcode.com/explore/challenge/card/october-leetcoding-challenge/559/week-1-october-1st-october-7th/3484/
fun main() {
    val victim = ComplementOfBase10Integer()
    println(victim.bitwiseComplement(5))
    println(victim.bitwiseComplement(7))
    println(victim.bitwiseComplement(10))
}
class ComplementOfBase10Integer {
    fun bitwiseComplement(N: Int): Int {
        if (N == 0) {
            return 1
        }
        val binaryStr = StringBuilder()

        var decimalNumber = N
        while (decimalNumber > 0) {
            val r = decimalNumber % 2
            decimalNumber /= 2

            binaryStr.append(if (r == 1) 0 else 1)
        }
        return Integer.parseInt(binaryStr.reversed().toString(), 2);
    }
}