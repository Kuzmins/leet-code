package lv.germans.leetcode.october.week1

//https://leetcode.com/explore/challenge/card/october-leetcoding-challenge/559/week-1-october-1st-october-7th/3483/
fun main() {
    println(removeCoveredIntervals(arrayOf(
            intArrayOf(1, 2),
            intArrayOf(1, 4),
            intArrayOf(3, 4))
    ))

}

fun removeCoveredIntervals(intervals: Array<IntArray>): Int {

    intervals.sortWith(compareBy<IntArray> { it[0] }.thenByDescending { it[1] })
    var elementsToRemove = 0;
    var target: IntArray? = null
    for (interval in intervals) {
        if (target == null) {
            target = interval
            continue
        }

        if (interval.isCoveredBy(target)) {
            elementsToRemove++
        } else {
            target = interval
        }
    }

    return intervals.size - elementsToRemove
}

private fun IntArray.isCoveredBy(otherArray: IntArray): Boolean {
    return this[0] >= otherArray[0] && this[1] <= otherArray[1]
}
