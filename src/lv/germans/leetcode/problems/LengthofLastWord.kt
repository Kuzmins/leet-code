package lv.germans.leetcode.problems

//https://leetcode.com/problems/length-of-last-word/
fun main() {
    val result = Solution().lengthOfLastWord("a ")
    println(result)
}

class Solution {
    fun lengthOfLastWord(s: String): Int {
        val string = s.trim()
        var counter = 0
        for (x in string.length - 1 downTo 0) {
            if (string[x] == ' ') {
                return counter
            } else {
                counter++
            }
        }
        return counter
    }
}

