package lv.germans.leetcode.september.week4

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/557/week-4-september-22nd-september-28th/3472/
fun main() {
    val victim = LargestNumber()
    println(victim.largestNumber(intArrayOf(10, 2)))
    println(victim.largestNumber(intArrayOf(3,30,34,5,9)))
    println(victim.largestNumber(intArrayOf(1)))
    println(victim.largestNumber(intArrayOf(10)))
}

class LargestNumber {

    fun largestNumber(nums: IntArray): String {
        val sorted = nums.map { it.toString() }.sortedWith(Comparator { a, b ->
            (b + a).compareTo(a + b)
        })
        return if (sorted.first() == "0") "0" else sorted.joinToString(separator = "")
    }
}