package lv.germans.leetcode.september.week4

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/557/week-4-september-22nd-september-28th/3470/
fun main() {
    val victim = GasStation()
    var gas = intArrayOf(1, 2, 3, 4, 5)
    var cost = intArrayOf(3, 4, 5, 1, 2)
    println(victim.canCompleteCircuit(gas, cost))

    gas = intArrayOf(2, 3, 4)
    cost = intArrayOf(3, 4, 3)
    println(victim.canCompleteCircuit(gas, cost))

}

class GasStation {
    fun canCompleteCircuit(gas: IntArray, cost: IntArray): Int {
        var total = 0
        var tank = 0
        var index = 0
        for (i in cost.indices) {
            val consume = gas[i] - cost[i]
            tank += consume
            if (tank < 0) {
                index = i + 1
                tank = 0
            }
            total += consume
        }
        return if (total < 0) -1 else index
    }
}