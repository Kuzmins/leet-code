package lv.germans.leetcode.september.week4

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/557/week-4-september-22nd-september-28th/3469/
fun main() {
    val victim = MajorityElementII()
    println(victim.majorityElement(intArrayOf(3, 2, 3)))
    println(victim.majorityElement(intArrayOf(1)))
    println(victim.majorityElement(intArrayOf(1, 2)))

}

class MajorityElementII {
    fun majorityElement(nums: IntArray): List<Int> {
        var firstCandidate: Int? = null
        var firstCounter = 0
        var secondCandidate: Int? = null
        var secondCounter = 0;
        for (i in nums) {
            if (i == firstCandidate) {
                firstCounter++
            } else if (i == secondCandidate) {
                secondCounter++
            }

            if (firstCounter == 0 && i != secondCandidate) {
                firstCandidate = i
                firstCounter++
            }

            if (secondCounter == 0 && i != firstCandidate) {
                secondCandidate = i
                secondCounter++
            }

            if (i != firstCandidate && i != secondCandidate) {
                firstCounter--
                secondCounter--
            }
        }

        firstCounter = 0
        secondCounter = 0
        for (i in nums) {
            if (i == firstCandidate) {
                firstCounter++
            }
            if (i == secondCandidate) {
                secondCounter++
            }
        }

        val result = mutableListOf<Int>()
        val target = nums.size / 3 + 1
        if (firstCounter >= target) {
            result.add(firstCandidate!!)
        }

        if (secondCounter >= target) {
            result.add(secondCandidate!!)
        }

        return result
    }
}