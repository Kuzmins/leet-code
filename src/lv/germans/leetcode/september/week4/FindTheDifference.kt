package lv.germans.leetcode.september.week4

fun main() {
    val victim = FindTheDifference()
    println(victim.findTheDifference("abcd", "abcde"))
    println(victim.findTheDifference("", "y"))
    println(victim.findTheDifference("a", "aa"))
    println(victim.findTheDifference("ae", "aea"))
}

class FindTheDifference {
    fun findTheDifference(s: String, t: String): Char {
        val firstCharSum = s.sumBy(Char::toInt)
        val secondCharSum = t.sumBy(Char::toInt)
        val result = secondCharSum - firstCharSum
        return result.toChar()
    }
}