package lv.germans.leetcode.september.week3

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/556/week-3-september-15th-september-21st/3466/
fun main() {
    val victim = UniquePathsIII()
    val grid1 = arrayOf(
            intArrayOf(1, 0, 0, 0),
            intArrayOf(0, 0, 0, 0),
            intArrayOf(0, 0, 2, -1)
    )
    println(victim.uniquePathsIII(grid1))

    val grid2 = arrayOf(
            intArrayOf(1, 0, 0, 0),
            intArrayOf(0, 0, 0, 0),
            intArrayOf(0, 0, 0, 2)
    )
    println(victim.uniquePathsIII(grid2))

}

class UniquePathsIII {
    var totalPath = 0;
    var totalZeroes = 0;
    fun uniquePathsIII(grid: Array<IntArray>): Int {
        totalPath = 0;
        totalZeroes = 0;

        val scanResult = scanArray(grid);
        val startIndex: Pair<Int, Int> = scanResult.first
        totalZeroes = scanResult.third
        val arr = Array(grid.size) { IntArray(grid.first().size) { 0 } }
        makeMove(grid, startIndex, arr, 0)
        return totalPath;
    }

    private fun makeMove(grid: Array<IntArray>, startIndex: Pair<Int, Int>, visited: Array<IntArray>, zeroCounter: Int): Int {
        val rowSize = grid.size
        val columnsSize = grid.first().size


        if (grid[startIndex.first][startIndex.second] == 2) {
            if (zeroCounter == totalZeroes) {
                totalPath += 1
            }
            return zeroCounter
        }

        if (grid[startIndex.first][startIndex.second] == -1) {
            return zeroCounter
        }

        if (visited[startIndex.first][startIndex.second] == 1) {
            return zeroCounter
        }

        var newZeroCounter = zeroCounter
        if (grid[startIndex.first][startIndex.second] == 0) {
            newZeroCounter++
        }

        visited[startIndex.first][startIndex.second] = 2

        visited[startIndex.first][startIndex.second] = 1

        if (startIndex.first < rowSize - 1) {
            makeMove(grid, Pair(startIndex.first + 1, startIndex.second), visited.copy(), newZeroCounter)
        }
        if (startIndex.second < columnsSize - 1) {
            makeMove(grid, Pair(startIndex.first, startIndex.second + 1), visited.copy(), newZeroCounter)
        }

        if (startIndex.first > 0) {
            makeMove(grid, Pair(startIndex.first - 1, startIndex.second), visited.copy(), newZeroCounter)
        }

        if (startIndex.second > 0) {
            makeMove(grid, Pair(startIndex.first, startIndex.second - 1), visited.copy(), newZeroCounter)
        }
        return newZeroCounter;
    }

    private fun scanArray(grid: Array<IntArray>): Triple<Pair<Int, Int>, Pair<Int, Int>, Int> {
        var startIndex: Pair<Int, Int> = Pair(-1, -1)
        var endIndex: Pair<Int, Int> = Pair(-1, -1)
        var zeroCounter = 0

        for ((rowIndex, row) in grid.withIndex()) {
            for ((columnIndex, column) in row.withIndex()) {
                if (column == 1) {
                    startIndex = Pair(rowIndex, columnIndex);
                }
                if (column == 2) {
                    endIndex = Pair(rowIndex, columnIndex);
                }
                if (column == 0) {
                    zeroCounter++
                }
            }
        }
        return Triple(startIndex, endIndex, zeroCounter)
    }

    fun Array<IntArray>.copy() = map { it.clone() }.toTypedArray()
}