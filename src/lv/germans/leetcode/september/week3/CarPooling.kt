package lv.germans.leetcode.september.week3

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/556/week-3-september-15th-september-21st/3467/
fun main() {
    val victim = CarPooling()
    println(victim.carPooling(trips = arrayOf(intArrayOf(2, 1, 5), intArrayOf(3, 3, 7)), capacity = 4))
    println(victim.carPooling(trips = arrayOf(intArrayOf(2, 1, 5), intArrayOf(3, 3, 7)), capacity = 5))
    println(victim.carPooling(trips = arrayOf(intArrayOf(2, 1, 5), intArrayOf(3, 5, 7)), capacity = 3))
    println(victim.carPooling(trips = arrayOf(intArrayOf(3, 2, 7), intArrayOf(3, 7, 9), intArrayOf(8, 3, 9)), capacity = 11))
}

class CarPooling {

    fun carPooling(trips: Array<IntArray>, capacity: Int): Boolean {
        trips.sortWith(compareBy({ it[1] }, { it[2] }))
        var passengersInCar = 0;
        val dropList = mutableMapOf<Int, Int>()
        for (trip in trips) {
            dropList[trip[2]] = trip[0] + (dropList[trip[2]] ?: 0)
            var passengerToDrop = 0

            dropList.filterKeys { it <= trip[1] }.forEach {
                passengerToDrop += it.value
                dropList.remove(it.key)
            }

            passengersInCar -= passengerToDrop

            passengersInCar += trip[0]
            if (passengersInCar > capacity) {
                return false
            }

        }
        return true
    }
}