package lv.germans.leetcode.september.week3;

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/556/week-3-september-15th-september-21st/3464/
public class BestTimeToBuyAndSellStock {

    public static void main(String[] args) {
        BestTimeToBuyAndSellStock victim = new BestTimeToBuyAndSellStock();
        System.out.println(victim.maxProfit(new int[]{7, 1, 5, 3, 6, 4}));
        System.out.println(victim.maxProfit(new int[]{7,6,4,3,1}));
    }

    public int maxProfit(int[] prices) {
        int max = 0;
        for (int i = 0; i < prices.length; i++) {
            int buyDayPrice = prices[i];
            for (int j = i + 1; j < prices.length; j++) {
                int sellDayPrice = prices[j];
                int proffit = sellDayPrice - buyDayPrice;
                if (proffit > max) {
                    max = proffit;
                }
            }
        }
        return max;
    }
}
