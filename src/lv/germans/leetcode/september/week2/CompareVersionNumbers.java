package lv.germans.leetcode.september.week2;

import java.math.BigDecimal;

//https://leetcode.com/explore/challenge/card/september-leetcoding-challenge/555/week-2-september-8th-september-14th/3454/
public class CompareVersionNumbers {

    public static void main(String[] args) {
        CompareVersionNumbers victim = new CompareVersionNumbers();
        System.out.println(victim.compareVersion("1.01", "1.001"));
        System.out.println(victim.compareVersion("1.0", "1.0.0"));
        System.out.println(victim.compareVersion("0.1", "1.1"));
        System.out.println(victim.compareVersion("1.0.1", "1"));
        System.out.println(victim.compareVersion("7.5.2.4", "7.5.3"));
    }

    public int compareVersion(String version1, String version2) {
        return calulateWeight(version1).compareTo(calulateWeight(version2));
    }

    private BigDecimal calulateWeight(String version) {
        String[] split = version.split("\\.");
        BigDecimal base = new BigDecimal("0.1");
        BigDecimal summ = BigDecimal.ZERO;
        for (int i = 1; i < split.length + 1; i ++) {
            summ = summ.add(new BigDecimal(split[i - 1]).multiply(base.pow(i)));
        }
        return summ;
    }
}
